<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->text('program')->nullable();
            $table->string('start_date')->nullable();
            $table->string('end_date')->nullable();
            $table->boolean('onetime')->default( false );
            $table->string('event_key')->nullable();

            // for assigning defferent target participants.
            $table->enum('target',[
                'students', // College
                'officers', // Organization
                'individual' // Students
            ])->nullable();

            $table->integer('college_id')->nullable()->unsigned(); // If event is exclusive for a specific college
            $table->integer('target_organization')->nullable()->unsigned(); // If event is exclusive for a specific organization
            $table->text('students')->nullable(); // for individual students pointed out specifically.

            // Foregn Keys
            $table->integer('user_id')->nullable()->unsigned(); // ID of the event creator
            $table->integer('organization_id')->nullable()->unsigned(); // ID of the organization incharge
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
