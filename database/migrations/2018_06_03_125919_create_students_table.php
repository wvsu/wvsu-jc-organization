<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {

            $table->increments('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->integer('year_level')->nullable()->unsigned();
            $table->string('year_section')->nullable();
            $table->text('hash_code')->nullable(); // QR code hash owned by the student
            $table->text('about_me')->nullable();
            $table->string('birth_date')->nullable(); // dd-mm-Y
            $table->string('mobile_number')->nullable(); // without prefix
            $table->string('student_id')->nullable(); // Student ID given by the school

            // Foreign keys
            $table->integer('officer_id')->nullable()->unsigned(); // if student is an officer
            $table->integer('college_id')->unsigned(); // student college
            $table->integer('user_id')->nullable()->unsigned(); // student user account
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
