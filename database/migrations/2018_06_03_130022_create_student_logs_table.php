<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('log_type', ['login','logout','onetime'])->nullable();

            // Foregn keys
            $table->integer('student_id')->nullable()->unsigned(); // ID of the target student
            $table->integer('organization_id')->nullable()->unsigned(); // ID of the spearhead org.
            $table->integer('college_id')->nullable()->unsigned(); // ID of the college
            $table->integer('event_id')->nullable()->unsigned(); // ID of the event
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_logs');
    }
}
