<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$role_employee = new Role();
    	$role_employee->name = 'admin';
    	$role_employee->description = 'Have access to everything.';
    	$role_employee->save();

    	$role_manager = new Role();
    	$role_manager->name = 'faculty';
    	$role_manager->description = 'Has basic admin privilages.';
    	$role_manager->save();

        $role_manager = new Role();
        $role_manager->name = 'officer';
        $role_manager->description = 'Has basic admin privilages.';
        $role_manager->save();

        $role_manager = new Role();
        $role_manager->name = 'student';
        $role_manager->description = 'Default user role.';
        $role_manager->save();
    }
}
