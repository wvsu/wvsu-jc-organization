<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$admin = new User();
        $admin->name = 'Administrator';
        $admin->first_name = 'Peter';
    	$admin->last_name = 'Porras';
    	$admin->email = 'admin@wvsu.com';
    	$admin->password = bcrypt('secret');
    	$admin->save();
    	$admin->roles()->attach( Role::where('name', 'admin')->first() );

    	$faculty = new User();
    	$faculty->name = 'Faculty';
        $faculty->first_name = 'Peter';
        $faculty->last_name = 'Porras';
    	$faculty->email = 'faculty@wvsu.com';
    	$faculty->password = bcrypt('secret');
    	$faculty->save();
    	$faculty->roles()->attach( Role::where('name', 'faculty')->first() );

        $officer = new User();
        $officer->name = 'Officer';
        $officer->first_name = 'Peter';
        $officer->last_name = 'Porras';
        $officer->email = 'officer@wvsu.com';
        $officer->password = bcrypt('secret');
        $officer->save();
        $officer->roles()->attach( Role::where('name', 'officer')->first() );

        $student = new User();
        $student->name = 'Student';
        $student->first_name = 'Peter';
        $student->last_name = 'Porras';
        $student->email = 'student@wvsu.com';
        $student->password = bcrypt('secret');
        $student->save();
        $student->roles()->attach( Role::where('name', 'student')->first() );
    }
}
