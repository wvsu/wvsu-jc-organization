<?php

use Illuminate\Database\Seeder;
use App\College;

class CollegeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $college = new College();
        $college->name = 'Information and Communications Technology';
        $college->description = 'This school mainly focuses on the study of technology and it\'s advancements.';
        $college->abrevation = 'ICT';
    	$college->save();

        $college = new College();
        $college->name = 'Hotel and Restaurant Services Technology';
        $college->description = 'This school mainly focuses on the study of hotel and restaurant management.';
        $college->abrevation = 'HRST';
        $college->save();

        $college = new College();
        $college->name = 'School of Teachers Education';
        $college->description = 'This school mainly focuses on the study of education and its branches.';
        $college->abrevation = 'ED';
        $college->save();

        $college = new College();
        $college->name = 'Health Care and Services Management';
        $college->description = 'This school mainly focuses on the study of health care.';
        $college->abrevation = 'BCM';
        $college->save();

        $college = new College();
        $college->name = 'Industryal Technology';
        $college->description = 'This school mainly focuses on the study of industryal technology.';
        $college->abrevation = 'IT';
        $college->save();
    }
}
