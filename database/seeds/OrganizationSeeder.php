<?php

use Illuminate\Database\Seeder;
use App\Organization;

class OrganizationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $org = new Organization();
        $org->name = 'Information Technology Students Society';
        $org->abrevation = 'ITSS';
        $org->description = "We, the students of Bachelor of Science in Information and Communication Technology of the West Visayas State University – Janiuay Campus, order to promote students welfare academic atmosphere, to develop and to instill and to promote a greater spirit of progress to our alma mater to ordain the establish this constitution and by-laws for the Information Technology Students’ Society.";
    	$org->save();
    }
}
