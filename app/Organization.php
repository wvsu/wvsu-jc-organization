<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'abrevation',
        'logo',
    ];

    public function events()
    {
        return $this->hasMany(
            'App\Event', 
            'organization_id', // foreign key
            'id' // local key
        );
    }

    public function logs()
    {
        return $this->hasMany(
            'App\StudentLog', 
            'organization_id', // foreign key
            'id' // local key
        );
    }

    public function officers()
    {
        return $this->hasMany(
            'App\Officer', 
            'organization_id', // foreign key
            'id' // local key
        );
    }
}
