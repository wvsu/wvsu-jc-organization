<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class College extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'abrevation',
        'logo',
    ];

    public function students()
    {
        return $this->hasMany(
            'App\Student', 
            'college_id', // foreign key
            'id' // local key
        );
    }

    public function events()
    {
        return $this->hasMany(
            'App\Event', 
            'college_id', // foreign key
            'id' // local key
        );
    }

    public function logs()
    {
        return $this->hasMany(
            'App\StudentLog', 
            'college_id', // foreign key
            'id' // local key
        );
    }
}
