<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\StudentLog;
use App\Event;


class Student extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 
        'last_name', 
        'middle_name',
        'year_level', 
        'year_section',
        'hash_code',
        'about_me',
        'birth_date',
        'mobile_number',
        'student_id',
        'college_id',
    ];

    public function user()
    {
        return $this->hasOne(
            'App\User', 
            'id', // foreign key
            'user_id' // local key
        )->first();
    }

    public function college()
    {
        return $this->hasOne(
            'App\College', 
            'id', // foreign key
            'college_id' // local key
        )->first();
    }

    public function officer()
    {
        return $this->hasOne(
            'App\Officer', 
            'student_id', // foreign key
            'id' // local key
        )->first();
    }

    public function logs()
    {
        return $this->hasMany(
            'App\StudentLog', 
            'student_id', // foreign key
            'id' // local key
        );
    }

    public function status( $event_id, $uid )
    {
        $event = Event::find($event_id);

        $onetime = StudentLog::where([
            ['event_id','=',$event_id],
            ['student_id','=',$uid],
            ['log_type','=','onetime']
        ])->first();

        if ( $onetime ) {
            
            return array(
                'status' => 'present',
                'login' => $onetime,
                'logout' => null,
            );
        }

        $logout = StudentLog::where([
            ['event_id','=',$event_id],
            ['student_id','=',$uid],
            ['log_type','=','logout']
        ])->first();

        $login = StudentLog::where([
            ['event_id','=',$event_id],
            ['student_id','=',$uid],
            ['log_type','=','login']
        ])->first();

        if ( $login && $logout ) {
            return array(
                'status' => 'present',
                'login' => $login,
                'logout' => $logout,
            );
        }  

        if ( $login && !$logout ) {

            return array(
                'status' => ( $event->onetime ) ? 'present':'loggedin',
                'login' => $login,
                'logout' => null,
            );
        }

        if ( !$login && !$logout ) {
            return array(
                'status' => 'absent',
                'login' => null,
                'logout' => null,
            );
        }
    }
}
