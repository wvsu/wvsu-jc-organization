<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Officer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'rank',       
        'student_id',
        'organization_id',
    ];

    public function student()
    {
        return $this->hasOne(
            'App\Student', 
            'id', // foreign key
            'student_id' // local key
        )->first();
    }

    public function org()
    {
        return $this->hasOne(
            'App\Organization', 
            'id', // foreign key
            'organization_id' // local key
        )->first();
    }
}
