<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'start_date',
        'end_date',
        'onetime',
        'organization_id',
        'target',
        'college_id',
        'target_organization',
        'students',
        'event_key',
    ];

    public function user()
    {
        return $this->hasOne(
            'App\User', 
            'id', // foreign key
            'user_id' // local key
        )->first();
    }

    public function logs()
    {
        return $this->hasMany(
            'App\StudentLog', 
            'event_id', // foreign key
            'id' // local key
        );
    }

    public function org()
    {
        return $this->hasOne(
            'App\Organization', 
            'id', // foreign key
            'organization_id' // local key
        )->first();
    }

    public function college()
    {
        return $this->hasOne(
            'App\College', 
            'id', // foreign key
            'college_id' // local key
        )->first();
    }

    public function target_org()
    {
        return $this->hasOne(
            'App\Organization', 
            'id', // foreign key
            'target_organization' // local key
        )->first();
    }
}
