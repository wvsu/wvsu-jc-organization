<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentLog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'log_type',
    ];

    public function student()
    {
        return $this->hasOne(
            'App\Student', 
            'id', // foreign key
            'student_id' // local key
        )->first();
    }

    public function org()
    {
        return $this->hasOne(
            'App\Organization', 
            'id', // foreign key
            'organization_id' // local key
        )->first();
    }

    public function college()
    {
        return $this->hasOne(
            'App\College', 
            'id', // foreign key
            'college_id' // local key
        )->first();
    }

    public function event()
    {
        return $this->hasOne(
            'App\Event', 
            'id', // foreign key
            'event_id' // local key
        )->first();
    }
}
