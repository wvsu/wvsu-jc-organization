<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Organization;
use Validator;
use Auth;


class OrganizationsController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->params = array(
            'title' => 'Organizations',
            'description' => 'Manage all university organizations.',
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {

        $search = ( $request->input('search') ) ? $request->input('search'): '';
        $this->params['search'] = $search;

        $organizations = Organization::orderBy('id','desc');
         
        if ( $search ) { $organizations->where('name', 'LIKE', "%$search%"); }

        $this->params['organizations'] = $organizations->paginate(10);

        return view('organizations.index', $this->params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->params['title'] = 'Create Organization';
        $this->params['description'] = 'Customize and register new organization.';

        return view('organizations.create', $this->params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['admin']);

        $validate = array(
            'name' => 'required|string|max:255',
            'abrevation' => 'required|string|max:255',
            'description' => 'required|string',
        );

        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) {
            return redirect('organizations/create')
                ->withErrors( $validator )
                ->withInput();
        }

        $organizations = new Organization();
        $organizations->fill( $request->all() );
        $organizations->save();

        return redirect('organizations')->with('success', $organizations->name . ' successfuly added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->params['title'] = 'Edit organization details.';
        $this->params['description'] = 'Update organization details.';

        $organization = Organization::find( $id );

        if ( ! $organization ) {
            return redirect('organizations')->with('warning', 'Organization no longer exist.');
        }

        $this->params['organization'] = $organization;

        return view('organizations.edit', $this->params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['admin']);

        $validate = array(
            'name' => 'required|string|max:255',
            'abrevation' => 'required|string|max:255',
            'description' => 'required|string',
        );

        $organization = Organization::find( $id );

        // double check if user exist.
        if ( ! $organization ) {
            return redirect('organizations')->with('error', 'Organization does not exist, please try again.');
        }

        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) {
            return redirect('organizations/'. $organization->id .'/edit')
                ->withErrors( $validator )
                ->withInput();
        }

        $organization->fill( $request->all() );
        $organization->save();

        return redirect('organizations/'. $organization->id .'/edit')->with('success', 'Organization details successfuly updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request, $id )
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['admin']);

        $organization = Organization::find( $id );

        if ( ! $organization ) {
            return response()->json([
                'error' => true,
                'message' => 'Please try again.'
            ]);
        }

        // Delete category
        $organization->delete();

        return response()->json([
            'error' => false,
            'message' => 'Organization successfuly removed.'
        ]);
    }
}
