<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StudentLog;
use App\Student;
use App\Event;
use QrCode;

class ApiController extends Controller
{
    // https://www.simplesoftware.io/docs/simple-qrcode#docs-usage
    public function qr( $size = 100, $format = 'png', $key )
    {
    	$size = preg_replace('/\D/', '', $size );
    	$size = ( empty( $size ) ) ? 100: $size;
    	$formats = array('png','eps','svg');
    	$format = ( in_array($format, $formats) ) ? $format:'png';
        return QrCode::format( $format )->size( $size )->generate( $key );
    }

    public function api_log( Request $request )
    {

        $response = array(
            'error' => false,
            'message' => '',
            'data' => '',
        );

        // Check if event key exist
        if ( !$request->input('key') ) {
            $response['error'] = true;
            $response['message'] = 'Event not found.';
            return response()->json( $response );
        }

        // Verify if event with designated key exist.
        $key = $request->input('key');
        $event = Event::where( 'event_key', '=', $key )->first();

        // Check if event exist.
        if ( ! $event ) {
            $response['error'] = true;
            $response['message'] = 'ERROR: Event not found.';
            return response()->json( $response );
        }

        // Check if event key exist
        if ( !$request->input('hash') ) {
            $response['error'] = true;
            $response['message'] = 'ERROR: Student not found.';
            return response()->json( $response );
        }

        // Find user who owns specific hash code.
        $student = Student::where( 'hash_code', '=', $request->input('hash') )->first();

        // Check if student exist.
        if ( ! $student ) {
            $response['error'] = true;
            $response['message'] = 'ERROR: Student not found.';
            return response()->json( $response );
        }

        // Check if login type
        if ( !$request->input('type') ) {
            $response['error'] = true;
            $response['message'] = 'ERROR: Select login type.';
            return response()->json( $response );
        }

        $logTypes = array('login','logout','onetime');
        if ( !in_array( $request->input('type'), $logTypes ) ) {
            $response['error'] = true;
            $response['message'] = 'ERROR: Select login type.';
            return response()->json( $response );
        }

        if ( $event->onetime ) {
            $logtype = 'onetime';
        } else {
            $logtype = strtolower( $request->input('type') );
        }

        $name = $student->first_name.' '.$student->last_name;

        if ( $logtype == 'logout' ) {

	        // Check if student already logged in.
	        $is_logged = StudentLog::where([
	            [ 'student_id', '=', $student->id ],
	            [ 'event_id', '=', $event->id ],
	            [ 'log_type', '=', 'login' ]
	        ])->count();

	        if ( $is_logged == 0 ) {
		        $response['error'] = true;
	            $response['message'] = 'ERROR: ' . $name . ' is not yet logged in.';
	            return response()->json( $response );
	        }
	    }

        // Check if student already logged in.
        $logCheck = StudentLog::where([
            [ 'student_id', '=', $student->id ],
            [ 'event_id', '=', $event->id ],
            [ 'log_type', '=', $logtype ]
        ])->count();

        // Check if there's a log record
        if ( $logCheck > 0 ) {
            
            $response['error'] = true;
            $response['message'] = 'SUCCESS: ' . $name . ' log already recorded.';

        } else {

            $log = new StudentLog();
            $log->log_type = $logtype; // login, logout, onetime
            $log->student_id = $student->id; 
            $log->organization_id = $event->org()->id;
            $log->college_id = $student->college()->id;
            $log->event_id = $event->id;
            $log->save();

            $response['message'] = 'SUCCESS: '.$name . ' log recorded!';
        }

        return response()->json( $response );
    }
}
