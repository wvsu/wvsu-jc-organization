<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Organization;
use App\StudentLog;
use App\ Officer;
use App\College;
use App\Student;
use App\Event;
use Validator;
use Auth;

class EventsController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->params = array(
            'title' => 'Events',
            'description' => 'Manage all university events.',
        );

        $this->validator = array(
            'name' => 'required|string|max:255',
            'description' => 'string',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
            'onetime' => 'boolean',
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $search = ( $request->input('search') ) ? $request->input('search'): '';
        $this->params['search'] = $search;

        $events = Event::orderBy('id','desc');
         
        if ( $search ) { $events->where('name', 'LIKE', "%$search%"); }

        $this->params['events'] = $events->paginate(10);

        return view('events.index', $this->params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        // Get the last event created and retrieve the ID
        $lastEvent = Event::orderBy('created_at', 'desc')->first();
        
        $this->params['title'] = 'Create event';
        $this->params['description'] = 'Customize and create new event.';
        $this->params['organizations'] = Organization::orderBy('name','desc')->get();
        $this->params['colleges'] = College::orderBy('name','desc')->get();

        $append = ( $lastEvent ) ? ($lastEvent->id+1): 0;

        // Generate a unique ID for th event.
        $this->params['event_key'] = $this->generateRandomString( 5 ) . '' . $append;

        return view('events.create', $this->params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['admin']);

        $datas = $request->all();

        $start_time = $request->input('start_time') ? $request->input('start_time'): '00:00:00';
        $end_time = $request->input('end_time') ? $request->input('end_time'):'00:00:00';

        if ( $request->input('start_date') ) {
            $datas['start_date'] = date( 'Y-m-d H:i:s', strtotime( $request->input('start_date').' '.$start_time ) );
        }

        if ( $request->input('end_date') ) {
            $datas['end_date'] = date( 'Y-m-d H:i:s', strtotime( $request->input('end_date').' '.$end_time ) );   
        }

        // check if organization exist.
        if ( ! Organization::find( $datas['organization_id'] ) ) {
            return redirect('events/create')->with('warning', 'Organization no longer exist.');
        }

        $validator = Validator::make( $datas, $this->validator );

        if ( $validator->fails() ) {
            return redirect('events/create')
                ->withErrors( $validator )
                ->withInput();
        }

        $events = new event();
        $events->fill( $datas );

        if ( $request->input('college_id') ) {
            $events->college_id = $request->input('college_id');
        }

        $events->user_id = Auth::user()->id;
        $events->save();

        return redirect('events')->with('success', $events->name . ' successfuly added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( Request $request, $id )
    {
        $this->params['title'] = 'Event Details';
        $this->params['description'] = 'View all event information and progess';
        $this->params['statusClass'] = array(
            'present' => 'success',
            'loggedin' => 'info',
            'absent' => 'danger',
        );
        
        $event = Event::find( $id );

        if ( ! $event ) {
            return redirect('events')->with('warning', 'Event no longer exist.');
        }

        $is_done  = ( strtotime( $event->end_date ) < strtotime( date('Y-m-d H:i:s') ) ); 
        $is_today = ( strtotime( $event->end_date ) == strtotime( date('Y-m-d H:i:s') ) ); 
        $success  = ( $is_done ) ? 'default' : 'success';
        $status   = ( $is_done ) ? 'Finished' : 'Upcoming';

        if ( $is_today ) {
            $success = 'primary';
            $status  = 'Active';
        }

        $search     = ( $request->input('search') ) ? $request->input('search'): '';
        $perpage    = preg_replace('/\D/', '', $request->input('perpage') );
        $perpage    = ( empty( $perpage ) || $perpage == 0 ) ? 10: $perpage;
        $levels     = array(1,2,3,4);
        $sections   = array('A','B','C','D');
        $level      = ( $request->input('year_level') ) ? $request->input('year_level'): '';
        $section    = ( $request->input('year_section') ) ? $request->input('year_section'): '';
        $level      = ( in_array( $level, $levels ) ) ? $level: '';
        $section    = ( in_array( $section, $sections ) ) ? $section: '';
        $college_id = preg_replace('/\D/', '', $request->input('college') );

        // If the event have specific college
        $students = Student::orderBy('year_level','asc');

        $where = [];

        switch ( $event->target ) {

            case 'students':

                $where[] = [ 'college_id', '=', $event->college_id ];
                break;

            case 'officers':
                
                // Get all officer Student ID's 
                $officer_ids = Officer::where( 'organization_id','=', $event->target_organization )
                    ->pluck( 'student_id' )
                    ->all();

                // Get all officers under students table.
                $students->whereIn( 'id', $officer_ids );

                break;

            case 'individual':
                
                // decode array values
                $student_ids = json_decode( $event->students );

                if ( is_array( $student_ids ) ) {
                    $students->whereIn( 'id', $student_ids );
                }

                break;
        }

        // Check for search filters.
        if ( ! empty( $level ) ) { $where[] = [ 'year_level', '=', $level ]; }
        if ( ! empty( $section ) ) { $where[] = [ 'year_section', '=', $section ]; }

        // Check if search filter for college is available
        if ( is_null( $event->college_id ) ) {
            // Include the college ID in the search query.
            if ( ! empty( $college_id ) ) { $where[] = ['college_id', '=', $college_id]; }
        }

        // Add this filter if event is exclusive
        if ( ! is_null( $event->college_id ) ) {
            $where[] = ['college_id', '=', $event->college_id];
        }

        // Check if keyword in search field is not empty.
        if ( $search ) { 

            $where[] = ['first_name', 'LIKE', "%$search%"];

            $students->where( $where );
            $students->orWhere('last_name', 'LIKE', "%$search%");
            $students->orWhere('student_id', 'LIKE', "%$search%");

        } else {

            $students->where( $where );
        }

        $args = $request->all();
        $args['tab'] = 'tab-2';

        // Pull all target student ID's
        $student_ids = $students->pluck('id')->all();

        $login_type = ( $event->onetime ) ? 'onetime':'login';

        // ID's of students who logged in.
        $login_ids = StudentLog::where([
            ['event_id', '=', $event->id],
            ['log_type', '=', $login_type],
        ])->pluck('student_id')->all();

        // check number of students who logged out.
        $loggedout = 0;
        $loggedout_percentage = 0;
        $no_logout = 0;

        $students = $students->paginate( $perpage );
        $total_students = $students->total();

        if ( $event->onetime ) { 

            $loggedout = StudentLog::where([
                ['event_id', '=', $event->id],
                ['log_type', '=', $login_type],
            ])->count();

        } else {
            
            // ID's of students who logged out.
            $logout_ids = StudentLog::where([
                ['event_id', '=', $event->id],
                ['log_type', '=', 'logout'],
            ])->pluck('student_id')->all();

            $no_logout = array_diff( $login_ids, $logout_ids );
            $no_logout = count( $no_logout );
            
            // check number of students who logged out.
            $loggedout = count( $logout_ids );

            $loggedout_percentage = ( $no_logout * 100 ) / $total_students;
            $loggedout_percentage = $loggedout_percentage; //ceil( $loggedout_percentage );
        }

        // check number of students who logged in
        $present = $loggedout - $no_logout;

        $present_percentage = ( $present * 100 ) / $total_students;
        $this->params['present_percentage'] = $present_percentage;//ceil( $present_percentage );

        $this->params['loggedout_percentage'] = $loggedout_percentage;

        $absent = ($total_students - $present) - $no_logout;
        $absent_percentage = ( $absent * 100 ) / $total_students;
        $this->params['absent_percentage'] = $absent_percentage; //ceil( $absent_percentage );

        $this->params['colleges']   = College::orderBy('name','desc')->get();
        $this->params['college_id'] = $college_id;
        $this->params['search']     = $search;
        $this->params['perpage']    = $perpage;
        $this->params['level']      = $level;
        $this->params['section']    = $section;
        $this->params['args']       = $args;
        $this->params['tab']        = ( $request->input('tab') ) ? $request->input('tab'):'';
        $this->params['students']   = $students;
        $this->params['event']      = $event;
        $this->params['status']     = $status;
        $this->params['class']      = $success;

        return view('events.show', $this->params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->params['title'] = 'Edit event details.';
        $this->params['description'] = 'Update event details.';
        $this->params['organizations'] = Organization::orderBy('name','desc')->get();
        $this->params['colleges'] = College::orderBy('name','desc')->get();

        $event = Event::find( $id );

        if ( ! $event ) {
            return redirect('events')->with('warning', 'event no longer exist.');
        }

        $this->params['event'] = $event;

        return view('events.edit', $this->params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['admin']);

        $event = Event::find( $id );

        // double check if user exist.
        if ( ! $event ) {
            return redirect('events')->with('error', 'Event no longer exist, please try again.');
        }

        $datas = $request->all();
        $datas['onetime'] = $request->input('onetime') ? true : false;

        $start_time = $request->input('start_time') ? $request->input('start_time'): '00:00:00';
        $end_time = $request->input('end_time') ? $request->input('end_time'):'00:00:00';

        if ( $request->input('start_date') ) {
            $datas['start_date'] = date( 'Y-m-d H:i:s', strtotime( $request->input('start_date').' '.$start_time ) );
        }

        if ( $request->input('end_date') ) {
            $datas['end_date'] = date( 'Y-m-d H:i:s', strtotime( $request->input('end_date').' '.$end_time ) );   
        }

        // check if organization exist.
        if ( ! Organization::find( $datas['organization_id'] ) ) {
            return redirect('events/'.$event->id.'/edit')->with('warning', 'Organization no longer exist.');
        }

        $validator = Validator::make( $datas, $this->validator );

        if ( $validator->fails() ) {
            return redirect('events/'.$event->id.'/edit')
                ->withErrors( $validator )
                ->withInput();
        }

        if ( $request->input('target') == 'individual' ) {
            $datas['students'] = json_encode( $request->input('students') );
        }

        $event->fill( $datas );

        if ( $request->input('college_id') ) { 
            $event->college_id = $request->input('college_id'); 
        } else {
            $event->college_id = null;
        }

        $event->save();

        return redirect('events/'. $event->id .'/edit')->with('success', 'Event details successfuly updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request, $id )
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['admin']);

        $event = Event::find( $id );

        if ( ! $event ) {
            return response()->json([
                'error' => true,
                'message' => 'Please try again.'
            ]);
        }

        // Delete category
        $event->delete();

        return response()->json([
            'error' => false,
            'message' => 'Event successfuly removed.'
        ]);
    }

    public function generateRandomString( $length = 10 ) {

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen( $characters );
        $string = '';

        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[rand(0, $charactersLength - 1)];
        }

        return $string;
    }
}
