<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Student;
use App\College;
use Validator;
use Auth;

class StudentsController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->params = array(
            'title' => 'Students',
            'description' => 'Manage all university students.',
            'sections' => array('A','B','C','D'),
            'levels' => array(
                1 => '1st year',
                2 => '2nd year',
                3 => '3rd year',
                4 => '4th year',
            ),
        );

        $this->validator = array(
            'first_name'    => 'required|string|max:255',
            'last_name'     => 'required|string|max:255',
            'year_level'    => 'required|integer|max:12',
            'year_section'  => 'required|string',
            'student_id'    => 'required|string|max:255',
            'college_id'    => 'required|integer',
            
            'about_me'      => 'nullable|string',
            'birth_date'    => 'nullable|string',
            'mobile_number' => 'nullable|string',
            
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {

        $search = ( $request->input('search') ) ? $request->input('search'): '';
        $perpage = preg_replace('/\D/', '', $request->input('show') );
        $college_id = preg_replace('/\D/', '', $request->input('college') );
        $show = ( empty( $perpage ) || $perpage == 0 ) ? 10: $perpage;
        $level   = ( $request->input('year_level') ) ? $request->input('year_level'): '';
        $section = ( $request->input('year_section') ) ? $request->input('year_section'): '';

        $students = Student::orderBy('year_level','asc');

        $where = [];

        // Check for search filters.
        if ( ! empty( $level ) ) { $where[] = [ 'year_level', '=', $level ]; }
        if ( ! empty( $section ) ) { $where[] = [ 'year_section', '=', $section ]; }
        if ( ! empty( $college_id ) ) { $where[] = [ 'college_id', '=', $college_id ]; }
        
        if ( $search ) { 

            $where[] = ['first_name', 'LIKE', "%$search%"];

            $students->where( $where );
            $students->orWhere('last_name', 'LIKE', "%$search%");
            $students->orWhere('student_id', 'LIKE', "%$search%");
            
        } else {

            $students->where( $where );
        }

        $this->params['search']     = $search;
        $this->params['perpage']    = $show;
        $this->params['colleges']   = College::orderBy('name','desc')->get();
        $this->params['college_id'] = $college_id;
        $this->params['students']   = $students->paginate( $show );
        $this->params['level']      = $level;
        $this->params['section']    = $section;

        return view('students.index', $this->params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->params['title'] = 'Add new student';
        $this->params['description'] = 'Customize and register new student.';
        $this->params['colleges'] = College::all();

        return view('students.create', $this->params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Block users who are not admin
        $request->user()->authorizeRoles([ 'admin', 'officer', 'faculty' ]);

        $validator = Validator::make( $request->all(), $this->validator );

        if ( $validator->fails() ) {
            return redirect('students/create')
                ->withErrors( $validator )
                ->withInput();
        }

        $student = new Student();
        $student->fill( $request->all() );
        $student->hash_code = md5( $request->input('first_name') . ' ' . $request->input('last_name') );
        $student->save();

        return redirect('students')->with('success', $student->name . ' successfuly added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->params['title'] = 'Edit student details.';
        $this->params['description'] = 'Update student details.';
        $this->params['colleges'] = College::all();

        $student = Student::find( $id );

        if ( ! $student ) {
            return redirect('students')->with('warning', 'Student no longer exist.');
        }

        $this->params['student'] = $student;

        return view('students.edit', $this->params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['admin','officer','faculty']);

        $student = Student::find( $id );

        // double check if user exist.
        if ( ! $student ) {
            return redirect('students')->with('error', 'Student does not exist, please try again.');
        }

        $validator = Validator::make( $request->all(), $this->validator );

        if ( $validator->fails() ) {
            return redirect('students/'. $student->id .'/edit')
                ->withErrors( $validator )
                ->withInput();
        }

        $student->fill( $request->all() );
        $student->hash_code = md5( $request->input('first_name').' '.$request->input('last_name') );
        $student->save();

        return redirect('students/'. $student->id .'/edit')->with('success', 'Student details successfuly updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request, $id )
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['admin','officer','faculty']);

        $student = Student::find( $id );

        if ( ! $student ) {
            return response()->json([
                'error' => true,
                'message' => 'Please try again.'
            ]);
        }

        // Delete category
        $student->delete();

        return response()->json([
            'error' => false,
            'message' => 'Student successfuly removed.'
        ]);
    }
}
