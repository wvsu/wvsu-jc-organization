<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\College;
use Validator;
use Auth;

class CollegesController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->params = array(
            'title' => 'Colleges',
            'description' => 'Manage all colleges.',
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {

        $search = ( $request->input('search') ) ? $request->input('search'): '';
        $this->params['search'] = $search;

        if ( Auth::user()->hasRole('admin') ) {
            $colleges = College::orderBy('id','desc');
        } else {
            $colleges = College::where('user_id', '=', Auth::user()->id)->orderBy('id','desc');
        }

        if ( $search ) { $colleges->where('name', 'LIKE', "%$search%"); }

        $this->params['colleges'] = $colleges->paginate(10);

        return view('colleges.index', $this->params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->params['title'] = 'Create new college';
        $this->params['description'] = 'Customize and register new college.';

        return view('colleges.create', $this->params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['admin']);

        $validate = array(
            'name' => 'required|string|max:255',
            'description' => 'required|string',
        );

        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) {
            return redirect('colleges/create')
                ->withErrors( $validator )
                ->withInput();
        }

        $colleges = new College();
        $colleges->fill( $request->all() );
        $colleges->save();

        return redirect('colleges')->with('success', $colleges->name . ' successfuly added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->params['title'] = 'Edit college info.';
        $this->params['description'] = 'Update college details.';

        $college = College::find( $id );

        if ( ! $college ) {
            return redirect('colleges')->with('warning', 'College no longer exist.');
        }

        $this->params['college'] = $college;

        return view('colleges.edit', $this->params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['admin']);

        $validate = array(
            'name' => 'required|string|max:255',
            'description' => 'required|string',
        );

        $college = College::find( $id );

        // double check if user exist.
        if ( ! $college ) {
            return redirect('colleges')->with('error', 'College does not exist, please try again.');
        }

        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) {
            return redirect('colleges/'. $college->id .'/edit')
                ->withErrors( $validator )
                ->withInput();
        }

        $college->fill( $request->all() );
        $college->save();

        return redirect('colleges/'. $college->id .'/edit')->with('success', 'College details successfuly updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request, $id )
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['admin']);

        $college = College::find( $id );

        if ( ! $college ) {
            return response()->json([
                'error' => true,
                'message' => 'Please try again.'
            ]);
        }

        // Delete category
        $college->delete();

        return response()->json([
            'error' => false,
            'message' => 'College successfuly removed.'
        ]);
    }
}
