<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StudentLog;
use App\College;
use App\Student;
use App\Event;
use QrCode;
use Route;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->params = array(
            'title' => 'Dashboard',
            'description' => 'Deafault landing page for admin',
        );
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( )
    {
        // dd( Route::current()->uri );
        return view('layouts.admin', $this->params);
    }

    public function student_ajax( Request $request )
    {

        $term = ( $request->input('term') ) ? $request->input('term'): '';
        $students = Student::orderBy('first_name','asc');

        if ( $term ) {
            $students->where('first_name', 'LIKE', "%$term%");
            $students->orWhere('last_name', 'LIKE', "%$term%");
            $students->orWhere('student_id', 'LIKE', "%$term%");
        }

        return response()->json( $students->get() );
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function print_qr( Request $request )
    {
        $search = ( $request->input('search') ) ? $request->input('search'): '';
        $perpage = preg_replace('/\D/', '', $request->input('show') );
        $college_id = preg_replace('/\D/', '', $request->input('college') );
        $show = ( empty( $perpage ) || $perpage == 0 ) ? 10: $perpage;
        $level   = ( $request->input('year_level') ) ? $request->input('year_level'): '';
        $section = ( $request->input('year_section') ) ? $request->input('year_section'): '';

        $students = Student::orderBy('year_level','asc');

        $where = [];

        // Check for search filters.
        if ( ! empty( $level ) ) { $where[] = [ 'year_level', '=', $level ]; }
        if ( ! empty( $section ) ) { $where[] = [ 'year_section', '=', $section ]; }
        if ( ! empty( $college_id ) ) { $where[] = [ 'college_id', '=', $college_id ]; }
        
        if ( $search ) { 

            $where[] = ['first_name', 'LIKE', "%$search%"];

            $students->where( $where );
            $students->orWhere('last_name', 'LIKE', "%$search%");
            $students->orWhere('student_id', 'LIKE', "%$search%");
            
        } else {

            $students->where( $where );
        }

        $this->params['search']     = $search;
        $this->params['perpage']    = $show;
        $this->params['colleges']   = College::orderBy('name','desc')->get();
        $this->params['college_id'] = $college_id;
        $this->params['students']   = $students->paginate( $show );
        $this->params['level']      = $level;
        $this->params['section']    = $section;
        

        return view('layouts.print', $this->params);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function print_students( Request $request )
    {
        $search = ( $request->input('search') ) ? $request->input('search'): '';
        $perpage = preg_replace('/\D/', '', $request->input('show') );
        $college_id = preg_replace('/\D/', '', $request->input('college') );
        $show = ( empty( $perpage ) || $perpage == 0 ) ? 10: $perpage;
        $level   = ( $request->input('year_level') ) ? $request->input('year_level'): '';
        $section = ( $request->input('year_section') ) ? $request->input('year_section'): '';

        $students = Student::orderBy('year_level','asc');

        $where = [];

        // Check for search filters.
        if ( ! empty( $level ) ) { $where[] = [ 'year_level', '=', $level ]; }
        if ( ! empty( $section ) ) { $where[] = [ 'year_section', '=', $section ]; }
        if ( ! empty( $college_id ) ) { $where[] = [ 'college_id', '=', $college_id ]; }
        
        if ( $search ) { 

            $where[] = ['first_name', 'LIKE', "%$search%"];

            $students->where( $where );
            $students->orWhere('last_name', 'LIKE', "%$search%");
            $students->orWhere('student_id', 'LIKE', "%$search%");
            
        } else {

            $students->where( $where );
        }

        $this->params['search']     = $search;
        $this->params['perpage']    = $show;
        $this->params['colleges']   = College::orderBy('name','desc')->get();
        $this->params['college_id'] = $college_id;
        $this->params['students']   = $students->paginate( $show );
        $this->params['level']      = $level;
        $this->params['section']    = $section;
        

        return view('layouts.print_students', $this->params);
    }
}
