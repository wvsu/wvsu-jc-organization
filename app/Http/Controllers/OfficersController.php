<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Organization;
use App\Officer;
use App\Student;
use Validator;

class OfficersController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->params = array(
            'title' => 'Officers',
            'description' => 'Manage all officers.',
        );

        $this->validator = array(
            'name'    => 'required|string|max:255',
            'rank'     => 'integer',
            'student_id' => 'integer',
            'organization_id' => 'integer',
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {

        $org = preg_replace('/\D/', '', $request->input('org') );
        $perpage = preg_replace('/\D/', '', $request->input('show') );
        $show = ( empty( $perpage ) || $perpage == 0 ) ? 10: $perpage;
        $this->params['org'] = $org;
        $this->params['perpage'] = $show;
        $this->params['organizations'] = Organization::all();

        $officers = Officer::orderBy('rank','asc');

        if ( $org ) { $officers->where( 'organization_id', '=', $org ); }

        $this->params['officers'] = $officers->paginate( $show );

        return view('officers.index', $this->params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( Request $request )
    {
        $org = preg_replace('/\D/', '', $request->input('org') );
        $this->params['rank'] = 0;

        if ( $org ) {
            $rank = Officer::where('organization_id','=', $org )->count();
            $this->params['rank'] = $rank+1;
        }
        

        $this->params['org'] = $org;
        $this->params['organizations'] = Organization::all();
        $this->params['title'] = 'Add new officer';
        $this->params['description'] = 'Customize and register new officer.';

        return view('officers.create', $this->params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Block users who are not admin
        $request->user()->authorizeRoles([ 'admin', 'officer', 'faculty' ]);

        $validator = Validator::make( $request->all(), $this->validator );

        if ( $validator->fails() ) {
            return redirect('officers/create')
                ->withErrors( $validator )
                ->withInput();
        }

        $student = Student::find( $request->input('student_id') );

        if ( ! $student ) {
        	return redirect('officers/create')
                ->with('warning', ' Student no longer exist.')
                ->withInput();
        }

        $org = Organization::find( $request->input('organization_id') );

        if ( ! $org ) {
        	return redirect('officers/create')
                ->with('warning', ' Organization no longer exist.')
                ->withInput();
        }

        $officer = new Officer();
        $officer->fill( $request->all() );
        $officer->save();

        return redirect('officers')->with('success', $officer->name . ' successfuly added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->params['organizations'] = Organization::all();
        $this->params['title'] = 'Edit student details.';
        $this->params['description'] = 'Update student details.';

        $officer = Officer::find( $id );

        if ( ! $officer ) {
            return redirect('officers')->with('warning', 'Officer no longer exist.');
        }

        $this->params['officer'] = $officer;

        return view('officers.edit', $this->params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['admin','officer','faculty']);

        $officer = Officer::find( $id );

        // double check if user exist.
        if ( ! $officer ) {
            return redirect('officers')->with('error', 'Officer does not exist, please try again.');
        }

        $validator = Validator::make( $request->all(), $this->validator );

        if ( $validator->fails() ) {
            return redirect('officers/'. $officer->id .'/edit')
                ->withErrors( $validator )
                ->withInput();
        }

        $officer->fill( $request->all() );
        $officer->save();

        return redirect('officers/'. $officer->id .'/edit')->with('success', 'Student details successfuly updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request, $id )
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['admin','officer','faculty']);

        $officer = Officer::find( $id );

        if ( ! $officer ) {
            return response()->json([
                'error' => true,
                'message' => 'Please try again.'
            ]);
        }

        // Delete category
        $officer->delete();

        return response()->json([
            'error' => false,
            'message' => 'Student successfuly removed.'
        ]);
    }
}
