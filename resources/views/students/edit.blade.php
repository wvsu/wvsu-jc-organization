@extends('layouts.admin')

@section('content')
<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Details<small class="m-l-sm"></small></h5>
		</div>
		<div class="ibox-content">
			
			<form action="{{ route( 'students.update', $student->id ) }}" method="POST" role="form">
				{{ csrf_field() }}
				<input type="hidden" name="_method" value="PUT">
				<div class="row">
					<div class="col-md-6">

						<div class="form-group">
							<label for="first_name">First Name</label>
							<input type="text" value="{{ $student->first_name }}" name="first_name" class="form-control" id="first_name" placeholder="First Name">
						</div>

						<div class="form-group">
							<label for="last_name">Last Name</label>
							<input type="text" value="{{ $student->last_name }}" name="last_name" class="form-control" id="last_name" placeholder="Last Name">
						</div>

						<div class="form-group">
							<label for="middle_name">Middle Name</label>
							<input type="text" value="{{ $student->middle_name }}" name="middle_name" class="form-control" id="middle_name" placeholder="Middle Name">
						</div>

						<div class="form-group" id="data_3">
                            <label class="font-noraml">Birth Day <i><small style="color: #ccc;">( Optional )</small></i></label>
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="birth_date" class="form-control" value="{{ $student->birth_date }}">
                            </div>
                        </div>

						<div class="form-group">
							<label for="about_me">About <i><small style="color: #ccc;">( Optional )</small></i></label>
							<textarea name="about_me" rows="5" class="form-control" id="about_me" placeholder="About">{{ $student->about_me }}</textarea>
						</div>

					</div>

					<div class="col-md-6">
						
						<div class="form-group">
							<label for="college_id">College</label>
							<select name="college_id" class="form-control" id="college_id">
								@foreach( $colleges as $college )
									<option value="{{ $college->id }}"{{ $student->college_id == $college->id ? ' selected':'' }}>{{ $college->name }}</option>
								@endforeach
							</select>
						</div>

						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label for="year_level">Year Level</label>
									<select name="year_level" class="form-control" id="year_level">
										@foreach( $levels as $level => $name )
											<option value="{{ $level }}"{{ $student->year_level == $level ? ' selected':'' }}>{{ $name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label for="year_section">Section</label>
									<select name="year_section" class="form-control" id="year_section">
										@foreach( $sections as $section )
											<option value="{{ $section }}"{{ $student->year_section == $section ? ' selected':'' }}>{{ $section }}</option>
										@endforeach
									</select>
								</div>	
							</div>
						</div>

						<div class="form-group">
							<label for="student_id">Student ID</label>
							<input type="text" value="{{ $student->student_id }}" name="student_id" class="form-control" id="student_id" placeholder="">
						</div>

						<div class="form-group">
							<label for="mobile_number">Mobile Number <i><small style="color: #ccc;">( Optional )</small></i></label>
							<input type="text" value="{{ $student->mobile_number }}" name="mobile_number" class="form-control" id="mobile_number" placeholder="09563965647">
						</div>

						<button type="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>

					</div>

				</div>
			
			</form>

		</div>
	</div>
</div>

@endsection

@section('action')
<div class="title-action">
<a href="{{ route('students.index', request()->input()) }}" class="btn btn-white"><i class="fa fa-chevron-left"></i> Back</a>
</div>
@endsection

@section('styles')
@endsection

@section('scripts')
@endsection

