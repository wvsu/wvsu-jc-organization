@extends('layouts.admin')

@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Students<small class="m-l-sm">Manage students.</small></h5>
		</div>
		<div class="ibox-content">
			
			<form action="{{ route('students.index') }}" method="GET" class="form-inline" role="form">

				<div class="input-group" style="margin-bottom: 15px;">
					<select class="form-control" name="college">
						<option value="">Select College</option>
						@foreach( $colleges as $college )
						<option value="{{ $college->id }}"{{ $college->id == $college_id ? ' selected':'' }}>{{ $college->name }}</option>
						@endforeach
					</select>
				</div>
				<br>

				<div class="input-group">
					<span class="input-group-addon">Per page</span> <input type="number" name="show" style="width: 80px;" placeholder="10" value="{{ $perpage }}" class="form-control">
				</div>

				<div class="input-group">
					<select class="form-control" name="year_level">
						<option value="">Year</option>
						<option value="1"{{ $level == '1' ? ' selected':'' }}>1st Year</option>
						<option value="2"{{ $level == '2' ? ' selected':'' }}>2nd Year</option>
						<option value="3"{{ $level == '3' ? ' selected':'' }}>3rd Year</option>
						<option value="4"{{ $level == '4' ? ' selected':'' }}>4th Year</option>
					</select>
				</div>

				<div class="input-group">
					<select class="form-control" name="year_section">
						<option value="">Section</option>
						<option value="A"{{ $section == 'A' ? ' selected':'' }}>Section A</option>
						<option value="B"{{ $section == 'B' ? ' selected':'' }}>Section B</option>
						<option value="C"{{ $section == 'C' ? ' selected':'' }}>Section C</option>
						<option value="D"{{ $section == 'D' ? ' selected':'' }}>Section D</option>
					</select>
				</div>

				<div class="input-group">
					<input type="text" name="search" value="{{ $search }}" placeholder="Name, Student ID" class="form-control">
					<span class="input-group-btn"> 
						<button type="submit" class="btn btn-info" style="margin: 0;">Search <i class="fa fa-search"></i></button>
					</span>
				</div>

				<div class="input-group pull-right">
            		<a href="{{ route('students.index') }}" class="btn btn-white">
						<i class="fa fa-refresh"></i> Reset Search
					</a>
					&nbsp;
					<a href="{{ route('print', request()->input()) }}" target="_blank" class="btn btn-white">
						<i class="fa fa-print"></i> Print QR Codes
					</a>
					&nbsp;
					<a href="{{ route('print_students', request()->input()) }}" target="_blank" class="btn btn-white">
						<i class="fa fa-print"></i> Print Students
					</a>
					
				</div>
			</form>
			<hr>

			<p class="label label-default">
				Students found: <b>{{ $students->total() }}</b>
			</p>

			<br><br>

			@if( $students->count() )

			<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover">
					<thead>
						<tr>
							<th>Student ID</th>
							<th>Name</th>
							<th>College</th>
							<th>Year & Section</th>
							<th>QR Code</th>
							<th>Options</th>
						</tr>
					</thead>
					<tbody>
						@foreach( $students as $student )
							<tr id="user-row-{{ $student->id }}">
								<td>{{ $student->student_id }}</td>
								<td>{{ $student->first_name.' '.$student->last_name }}</td>
								<td>{{ $student->college()->name }}</td>
								<td>{{ $student->year_level.'-'.$student->year_section }}</td>
								<td>
									<button 
										data-hash="{{ $student->hash_code }}" 
										data-name="{{ $student->first_name.' '.$student->last_name }}" 
										class="btn btn-info btn-xs" 
										data-toggle="modal" 
										data-target="#user-qrcode">
										<i class="fa fa-qrcode"></i> View QR Code
									</button>
								</td>
								<td>
									<a href="{{ route('students.edit', $student->id) }}" class="btn btn-success btn-xs">
										<i class="fa fa-edit"></i> Edit
									</a> | 
									<button 
										data-id="{{ $student->id }}" 
										data-cat="{{ $student->name }}" 
										data-name="delete" 
										class="btn btn-danger btn-xs" 
										data-toggle="modal" 
										data-target="#remove-user">
										<i class="fa fa-close"></i> Delete
									</button>
								</td>
							</tr>

						@endforeach
					</tbody>
				</table>
			</div>
			
			{{ $students->appends(request()->input())->links() }}

			@else 

			<div class="alert alert-warning">
				<strong>No records found,</strong> try again or start adding new students.
			</div>

			@endif

		</div>
	</div>
</div>

<div class="modal inmodal" id="remove-user" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Remove Student</h4>
            </div>
            <div class="modal-body">
                <p><strong>Are you sure?</strong> once removed, all details and information affiliated with this student will be deleted. This action is irreversible.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="pull-left btn btn-white"  data-dismiss="modal">Cancel</button>
                <button type="button" id="remove-btn" class="btn btn-danger">Delete</button>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="user-qrcode" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">QR CODE</h4>
            </div>
            <div class="modal-body" align="center">
	            <img id="qrcode-img" src="" class="img-responsive">	
	            <br>
	            <p id="user-info"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white"  data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('action')
<div class="title-action">
    <a href="{{ route('students.create') }}" class="btn btn-primary">Add Student <i class="fa fa-plus"></i></a>
</div>
@endsection

@section('styles')
<!-- Toastr style -->
<link href="{{asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- Toastr script -->
<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>
<script>
jQuery(document).ready(function() {

	toastr.options = {
		"closeButton": true,
		"debug": false,
		"progressBar": true,
		"positionClass": "toast-top-right",
		"onclick": null,
		"showDuration": "400",
		"hideDuration": "1000",
		"timeOut": "7000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}

	jQuery('button[data-name="delete"]').click(function(e) {
		e.preventDefault();
		var token = jQuery('meta[name="csrf-token"]').attr('content');
	});

	jQuery('#remove-user').on('show.bs.modal', function (event) {
		var button = jQuery(event.relatedTarget); // Button that triggered the modal
		var cat_id = button.data('id');
		jQuery('#remove-btn').data('id',cat_id);
  		jQuery(this).find('.modal-title').text('Remove ' + button.data('cat') );
	});

	jQuery('#user-qrcode').on('show.bs.modal', function (event) {
		var button = jQuery(event.relatedTarget); // Button that triggered the modal
		var hash = button.data('hash');
		jQuery('#qrcode-img').attr('src','/qr/250/png/'+hash);
  		jQuery(this).find('#user-info').text( button.data('name') );
	});

	jQuery('#remove-btn').click(function(e) {
		e.preventDefault();
		var cat_id = jQuery(this).data('id');
		var token = jQuery('meta[name="csrf-token"]').attr('content');

		jQuery.ajax({  
			url: '{{ route('students.index') }}/'+cat_id,
			type: 'POST',
			dataType: 'json',
			data: { _token: token, _method: 'DELETE' },
		})
		.always(function(data) {
			if ( data.error ) {
				toastr.error(data.message,'Error');
			} else {
				jQuery('#remove-user').modal('toggle');
				toastr.success(data.message,'Success');
				jQuery('#user-row-'+cat_id).remove();
			}
		});
		 
	});

});
</script>
@endsection
