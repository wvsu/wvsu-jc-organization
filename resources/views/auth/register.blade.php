@extends('layouts.public')


@section('content')
<div class="middle-box text-center loginscreen  animated fadeInDown">

    <div>
        <div>
            <h1 class="logo-name" style="font-size: 100px;">Register</h1>
        </div>
        <form class="m-t" method="post" role="form" action="{{ route('register') }}">
            {{ csrf_field() }}
            
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <input id="name" type="text" class="form-control" placeholder="Username" name="name" value="{{ old('name') }}" required autofocus>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input name="email" value="{{ old('email') }}" type="email" class="form-control" placeholder="Email" required="">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input name="password" type="password" class="form-control" placeholder="Password" required="">
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
            </div>

            <button type="submit" class="btn btn-primary block full-width m-b">Sign Up</button>

            <p class="text-muted text-center"><small>Already have an account?</small></p>
            <a class="btn btn-sm btn-white btn-block" href="{{ route('login') }}">Login to your account</a>
        </form>
        <p class="m-t"> <small>&copy; {{ date('Y') }}</small> </p>
    </div>

</div>
@endsection