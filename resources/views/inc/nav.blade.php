<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">

            <li class="nav-header">
                <div class="dropdown profile-element"> 
                    <span>
                        <img alt="image" class="img-circle" src="{{asset('img/profile_small.jpg')}}" />
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> 
                            <span class="block m-t-xs"> 
                                <strong class="font-bold">{{ Auth::user()->name }}</strong>
                            </span> 
                            <span class="text-muted text-xs block">Options <b class="caret"></b></span> 
                        </span> 
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="{{ url('settings') }}">Settings</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element"><i class="fa fa-bank"></i></div>
            </li>

            <li class="{{ Route::current()->uri == 'home' ? 'active':'' }}">
                <a href="{{ route('home') }}" data-toggle="tooltip" data-placement="right" title="Dashboard"> 
                    <i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span>
                </a>
            </li>

            @if ( Auth::user()->hasAnyRole(['admin','officer','faculty']) )

            <li class="{{ Route::current()->uri == 'events' ? 'active':'' }}">
                <a href="{{ route('events.index') }}" data-toggle="tooltip" data-placement="right" title="Events"> 
                    <i class="fa fa-calendar"></i> <span class="nav-label">Events</span>
                </a>
            </li>

            <li class="{{ Route::current()->uri == 'students' ? 'active':'' }}">
                <a href="{{ route('students.index') }}" data-toggle="tooltip" data-placement="right" title="Students"> 
                    <i class="fa fa-users"></i> <span class="nav-label">Students</span>
                </a>
            </li>

            <li class="{{ Route::current()->uri == 'organizations' || Route::current()->uri == 'officers' ? 'active':'' }}">
                <a href="#"><i class="fa fa-puzzle-piece"></i> <span class="nav-label">Organizations</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li class="{{ Route::current()->uri == 'organizations' ? 'active':'' }}">
                        <a href="{{ route('organizations.index') }}">Organization List</a>
                    </li>
                    <li class="{{ Route::current()->uri == 'officers' ? 'active':'' }}">
                        <a href="{{ route('officers.index') }}">Officers</a>
                    </li>
                </ul>
            </li>

            @endif

            @if ( Auth::user()->hasRole('admin') )

            <li class="{{ Route::current()->uri == 'colleges' ? 'active':'' }}">
                <a href="{{ route('colleges.index') }}" data-toggle="tooltip" data-placement="right" title="Colleges"> 
                    <i class="fa fa-graduation-cap"></i> <span class="nav-label">Colleges</span>
                </a>
            </li>
            <li class="{{ Route::current()->uri == 'users' ? 'active':'' }}">
                <a href="{{ route('users.index') }}" data-toggle="tooltip" data-placement="right" title="Users"> 
                    <i class="fa fa-users"></i> <span class="nav-label">Users</span>
                </a>
            </li>
            <li class="{{ Route::current()->uri == 'roles' ? 'active':'' }}">
                <a href="{{ route('roles.index') }}" data-toggle="tooltip" data-placement="right" title="User Roles"> 
                    <i class="fa fa-lock"></i> <span class="nav-label">User Roles</span>
                </a>
            </li>

            @endif

            <li class="{{ Route::current()->uri == 'settings' ? 'active':'' }}">
                <a href="{{ route('settings') }}" data-toggle="tooltip" data-placement="right" title="Settings">
                    <i class="fa fa-cogs"></i> <span class="nav-label">Profile Settings</span>
                </a>
            </li>

        </ul>
    </div>
</nav>