@extends('layouts.admin')

@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>College Information<small class="m-l-sm"></small></h5>
		</div>
		<div class="ibox-content">
			
			<form action="{{ route('colleges.store') }}" method="POST" role="form">
				{{ csrf_field() }}

				<div class="row">
					<div class="col-md-6">

						<div class="form-group">
							<label for="username">Name</label>
							<input type="text" value="{{ old('name') }}" name="name" class="form-control" id="name" placeholder="Name">
						</div>

						<div class="form-group">
							<label for="abrevation">Abrevation</label>
							<input type="text" value="{{ old('abrevation') }}" name="abrevation" class="form-control" id="abrevation" placeholder="BCM, SICT, HRST, etc..">
						</div>

						<div class="form-group">
							<label for="username">Description</label>
							<textarea name="description" class="form-control" rows="5" id="description" placeholder="Description">{{ old('description') }}</textarea>
						</div>



						<button type="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>

					</div>

				</div>
			
			</form>

		</div>
	</div>
</div>

@endsection

@section('action')
<div class="title-action">
<a href="{{ route('colleges.index') }}" class="btn btn-white"><i class="fa fa-chevron-left"></i> Back</a>
</div>
@endsection


@section('styles')
<link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
@endsection

@section('scripts')
@endsection