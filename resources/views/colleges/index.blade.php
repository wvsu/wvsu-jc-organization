@extends('layouts.admin')

@section('content')

<div class="col-md-12">

	<div class="ibox-content forum-container">

		<div class="forum-title">
			<div class="pull-right forum-desc">
				<samll>Total students: 320,800</samll>
			</div>
			<h3>University Colleges</h3>
		</div>

		@if( $colleges->count() )
		
		@foreach( $colleges as $college )
		
			<div class="forum-item active" id="user-row-{{ $college->id }}">
				<div class="row">
					<div class="col-md-9">
						<div class="forum-icon">
							<i class="fa fa-shield"></i>
						</div>
						<a href="#" class="forum-item-title">{{ $college->name }}</a>
						<div class="forum-sub-title">{{ $college->description }}</div>
					</div>
					<div class="col-md-1 forum-info">
						<span class="views-number">
							{{ number_format( $college->students->count(), 0, '.', ',' ) }}
						</span>
						<div>
							<small>Students</small>
						</div>
					</div>
					<div class="col-md-1 forum-info">
						<span class="views-number">
							<a href="{{ url("colleges/{$college->id}/edit") }}" class="btn pull-right btn-success btn-xs">
								<i class="fa fa-edit" style="color: #fff;"></i> Edit
							</a>
						</span>
					</div>
					<div class="col-md-1 forum-info">
						<span class="views-number">
							<button data-id="{{ $college->id }}" data-cat="{{ $college->name }}" data-name="delete" class="btn pull-right btn-danger btn-xs" data-toggle="modal" data-target="#remove-user">
								<i class="fa fa-close" style="color: #fff;"></i> Delete
							</button>
						</span>
						
					</div>
				</div><!-- row -->
			</div><!-- forum-item -->

		@endforeach


		{{ $colleges->appends(request()->input())->links() }}

		@else 

		<div class="alert alert-warning">
			<strong>No records!</strong> you may now start creating new colleges.
		</div>

		@endif


	</div>
</div>

<div class="modal inmodal" id="remove-user" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Remove College</h4>
            </div>
            <div class="modal-body">
                <p><strong>Are you sure?</strong> once removed, all students and events affiliated with this college will be deleted. This action is irreversible.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="pull-left btn btn-white"  data-dismiss="modal">Cancel</button>
                <button type="button" id="remove-btn" class="btn btn-danger">Delete</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('action')
<div class="title-action">
    <a href="{{ route('colleges.create') }}" class="btn btn-primary">Add college <i class="fa fa-plus"></i></a>
</div>
@endsection

@section('styles')
<!-- Toastr style -->
<link href="{{asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- Toastr script -->
<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>
<script>
jQuery(document).ready(function() {

	toastr.options = {
		"closeButton": true,
		"debug": false,
		"progressBar": true,
		"positionClass": "toast-top-right",
		"onclick": null,
		"showDuration": "400",
		"hideDuration": "1000",
		"timeOut": "7000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}

	jQuery('button[data-name="delete"]').click(function(e) {
		e.preventDefault();
		var token = jQuery('meta[name="csrf-token"]').attr('content');
	});

	jQuery('#remove-user').on('show.bs.modal', function (event) {
		var button = jQuery(event.relatedTarget); // Button that triggered the modal
		var cat_id = button.data('id');
		jQuery('#remove-btn').data('id',cat_id);
  		jQuery(this).find('.modal-title').text('Remove ' + button.data('cat') );
	});

	jQuery('#remove-btn').click(function(e) {
		e.preventDefault();
		var cat_id = jQuery(this).data('id');
		var token = jQuery('meta[name="csrf-token"]').attr('content');

		jQuery.ajax({  
			url: '{{ route('colleges.index') }}/'+cat_id,
			type: 'POST',
			dataType: 'json',
			data: { _token: token, _method: 'DELETE' },
		})
		.always(function(data) {
			if ( data.error ) {
				toastr.error(data.message,'Error');
			} else {
				jQuery('#remove-user').modal('toggle');
				toastr.success(data.message,'Success');
				jQuery('#user-row-'+cat_id).remove();
			}
		});
		 
	});

});
</script>
@endsection
