@extends('layouts.admin')

@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>User List<small class="m-l-sm">Manage all user accounts.</small></h5>
		</div>
		<div class="ibox-content">
			
			<form action="{{ route('users.index') }}" method="GET" class="form-inline" role="form">
				<div class="input-group">
					<span class="input-group-addon">Per page</span> <input type="number" name="show" style="width: 80px;" placeholder="10" value="{{ $perpage }}" class="form-control">
				</div>

				<div class="input-group">
					<input type="text" name="search" value="{{ $search }}" placeholder="username, email" class="form-control">
					<span class="input-group-btn"> 
						<button type="submit" class="btn btn-info" style="margin: 0;">Search <i class="fa fa-search"></i></button>
					</span>
				</div>
			</form>
			<hr>

			@if( $users->count() )
			
			<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover">
					<thead>
						<tr>
							<th>Username</th>
							<th>Email</th>
							<th>Roles</th>
							<th>Options</th>
						</tr>
					</thead>
					<tbody>
						@foreach( $users as $user )

							<tr id="user-row-{{ $user->id }}">
								<td>{{ $user->name }}</td>
								<td>{{ $user->email }}</td>
								<td>
									@if( $user->roles()->get() )
									@foreach( $user->roles()->get() as $role )
									<span class="badge badge-info">{{ $role->name }}</span>
									@endforeach
									@endif
								</td>
								<td>
									<a href="{{ url("users/{$user->id}/edit") }}" class="btn btn-success btn-xs">
										<i class="fa fa-edit"></i> Edit
									</a>
									@if( Auth::user()->id != $user->id ) 
									<button data-id="{{ $user->id }}" data-name="delete" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#remove-user">
										<i class="fa fa-close"></i> Remove
									</button>
									@endif
								</td>
							</tr>

						@endforeach
					</tbody>
				</table>
			</div>
			
			{{ $users->appends(request()->input())->links() }}
			@endif

		</div>
	</div>
</div>

<div class="modal inmodal" id="remove-user" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Remove User</h4>
            </div>
            <div class="modal-body">
                <p><strong>Are you sure?</strong> once removed, this user will no longer be able to login on his/her account and all data affiliated to this account will be deleted.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="pull-left btn btn-white"  data-dismiss="modal">Cancel</button>
                <button type="button" id="remove-btn" class="btn btn-danger">Remove</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('action')
<div class="title-action">
    <a href="{{ route('users.create') }}" class="btn btn-primary">Add User <i class="fa fa-plus"></i></a>
</div>
@endsection

@section('styles')
<!-- Toastr style -->
<link href="{{asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- Toastr script -->
<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>
<script>
jQuery(document).ready(function() {

	toastr.options = {
		"closeButton": true,
		"debug": false,
		"progressBar": true,
		"positionClass": "toast-top-right",
		"onclick": null,
		"showDuration": "400",
		"hideDuration": "1000",
		"timeOut": "7000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}

	jQuery('button[data-name="delete"]').click(function(e) {
		e.preventDefault();
		var token = jQuery('meta[name="csrf-token"]').attr('content');
	});

	jQuery('#remove-user').on('show.bs.modal', function (event) {
		var button = jQuery(event.relatedTarget); // Button that triggered the modal
		var user_id = button.data('id');
		jQuery('#remove-btn').data('id',user_id);
  		jQuery(this).find('.modal-title').text('Remove ' + jQuery('#user-row-'+user_id+' td:first').html() );
	});

	jQuery('#remove-btn').click(function(e) {
		e.preventDefault();
		var user_id = jQuery(this).data('id');
		var token = jQuery('meta[name="csrf-token"]').attr('content');

		jQuery.ajax({  
			url: '{{ route('users.index') }}/'+user_id,
			type: 'POST',
			dataType: 'json',
			data: { _token: token, _method: 'DELETE' },
		})
		.always(function(data) {
			if (data.error) {
				toastr.error(data.message,'Error');
			} else {
				jQuery('#remove-user').modal('toggle');
				toastr.success(data.message,'Success');
				jQuery('#user-row-'+user_id).remove();
			}
		});
		 
	});

});
</script>
@endsection
