<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Print QR CODES</title>
		<!-- Bootstrap CSS -->
		<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<style>p{
			margin-left: 20px;
    		margin-top: -25px;
		    font-size: 11px;
		}</style>
	</head>
	<body>
		
		<div class="row" id="print">
			<div class="col-md-12">
				@if( $students->count() )
				<div class="table-responsive">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>First Name</th>
									<th>Middle Name</th>
									<th>Last Name</th>
								</tr>
							</thead>
							<tbody>
								@foreach( $students as $student )
								<tr>
									<td>{{ $student->first_name }}</td>
									<td>{{ $student->middle_name }}</td>
									<td>{{ $student->last_name }}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				@endif
			</div>
		</div>
		<div></div>
		<script>
			// window.print();
			jQuery(document).ready(function($) {
				$('body div').last().remove();
			});
		</script>
	</body>
</html>