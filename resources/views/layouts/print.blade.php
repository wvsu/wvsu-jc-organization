<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Print QR CODES</title>
		<!-- Bootstrap CSS -->
		<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<style>p{
			margin-left: 20px;
    		margin-top: -25px;
		    font-size: 11px;
		}</style>
	</head>
	<body>
		
		<div class="row" id="print">
		@if( $students->count() )
		@foreach( $students as $student )
		<div class="col-md-2 col-sm-2 col-xs-12">
			<img src="/qr/250/png/{{ $student->hash_code }}" class="img-responsive" alt="">
			<p>{{ $student->first_name.' '.$student->middle_name.' '.$student->last_name }}</p>

		</div>
		@endforeach
		@endif
		</div>
		<script>
			window.print();
			jQuery(document).ready(function($) {
				$('body div').last().remove();
			});
		</script>
	</body>
</html>