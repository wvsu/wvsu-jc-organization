@extends('layouts.admin')

@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Add Event<small class="m-l-sm"></small></h5>
		</div>
		<div class="ibox-content">
			
			<form action="{{ route('events.store') }}" method="POST" role="form">
				{{ csrf_field() }}
				
				<div class="row">
					<div class="col-md-8">

						<div class="form-group">
							<label for="name">Event Name</label>
							<input type="text" value="{{ old('name') }}" name="name" class="form-control" id="name" placeholder="Name">
						</div>

						<div class="form-group">
							<label for="event_key">Event Key</label> <span><small>(<i>This key will be used when logging in students for verification.</i>)</small></span>
							<input type="hidden" value="{{ $event_key }}" name="event_key">
							<input type="text" value="{{ $event_key }}" class="form-control" disabled id="event_key" placeholder="Key">
						</div>

						<div class="form-group">
							<label for="description">Description</label>
							<textarea name="description" rows="8" class="form-control summernote" id="description" placeholder="Event Description">{{ old('description') }}</textarea>
						</div>
						
					</div>

					<div class="col-md-4">
						
						<div class="form-group">
							<label for="name">Event login only</label> <small>( <i>Disable event logout.</i> )</small><br>
							<input type="checkbox" name="onetime" value="1" class="js-switch" {{ old('onetime') ? 'checked':'' }} />
						</div>

						<div class="form-group">
							<label for="start_date">Start Date & time</label>
							<div class="input-group date">
                                <span class="input-group-addon">
                                	<i class="fa fa-calendar"></i>
                                </span>
                                <input type="text" value="{{ old('start_date') }}" placeholder="{{ date('m/d/Y') }}" name="start_date" id="start_date" class="form-control">
                                <span class="input-group-addon">
                                	<i class="fa fa-clock-o"></i>
                                </span>
                                <input type="time" name="start_time" class="form-control" value="{{ old('start_time') }}">
                            </div>
                            
						</div>

						<div class="form-group">
							<label for="end_date">End Date & time</label>
							<div class="input-group date">
                                <span class="input-group-addon">
                                	<i class="fa fa-calendar"></i>
                                </span>
                                <input type="text" value="{{ old('end_date') }}" placeholder="{{ date('m/d/Y') }}" name="end_date" id="end_date" class="form-control">
                                <span class="input-group-addon">
                                	<i class="fa fa-clock-o"></i>
                                </span>
                                <input type="time" name="end_time" class="form-control" value="{{ old('end_time') }}">
                            </div>
						</div>

						<div class="form-group">
							<label for="organization_id">Organization in charge</label>
							<select name="organization_id" class="form-control" id="organization_id">
								@foreach( $organizations as $org)
								<option value="{{ $org->id }}"{{ old('organization_id') == $org->id ? ' selected':'' }}>
									{{ $org->name }}
								</option>
								@endforeach
							</select>
						</div>

						<div class="form-group">
							<label for="college_id">Target Participants</label>

							<select name="target" class="form-control" id="target">
								<option value="students">College Students</option>
								<option value="officers">Organization Officers</option>
								<option value="individual">Specific Students</option>
							</select>

							<br>
							<label>Select</label>

							<select name="college_id" class="form-control" id="college_id">
								<option value="">All students</option>
								@foreach( $colleges as $school )
								<option value="{{ $school->id }}"{{ old('college_id') == $school->id ? ' selected':'' }}>
									{{ $school->abrevation }} Students Only
								</option>
								@endforeach
							</select>

							<select name="target_organization" class="form-control hidden" id="target_organization">
								@foreach( $organizations as $target_org )
								<option value="{{ $target_org->id }}"{{ old('target_organization') == $target_org->id ? ' selected':'' }}>
									{{ $target_org->name }}
								</option>
								@endforeach
							</select>

							<select name="students[]" data-placeholder="Select students..." class="form-control hidden" multiple id="students">
								<option value="">Select Students</option>
							</select>

						</div>
						
						<button type="submit" class="btn btn-primary">Publish Event <i class="fa fa-save"></i></button>
					</div>
				</div>
			
			</form>

		</div>
	</div>
</div>

@endsection

@section('action')
<div class="title-action">
<a href="{{ route('events.index') }}" class="btn btn-white"><i class="fa fa-chevron-left"></i> Back</a>
</div>
@endsection


@section('styles')
<link href="{{asset('css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
<link href="{{asset('css/plugins/summernote/summernote.css')}}" rel="stylesheet">
<link href="{{asset('css/plugins/summernote/summernote-bs3.css')}}" rel="stylesheet">
<link href="{{asset('css/plugins/switchery/switchery.css')}}" rel="stylesheet">
<link href="{{asset('css/plugins/chosen/chosen.css')}}" rel="stylesheet">
<style>
.note-editor {height: auto;}
.note-editor .note-editable{border: 1px solid #f5f5f5;}
</style>
@endsection

@section('scripts')
<script src="{{asset('js/plugins/summernote/summernote.min.js')}}"></script>
<script src="{{asset('js/plugins/datapicker/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('js/plugins/switchery/switchery.js')}}"></script>
<script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
<script>
	jQuery(document).ready(function() {
		var $=jQuery;
		$('.summernote').summernote();
		$('#start_date, #end_date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });
        var switchBtn = document.querySelector('.js-switch');
        var switchButton = new Switchery(switchBtn, { color: '#1AB394' });
        switchBtn.onchange = function() {
        	if ( switchBtn.checked ) {
        		$('.js-switch').attr('checked','checked');
        	} else {
        		$('.js-switch').removeAttr('checked');
        	}
		};

		$.getJSON('{{ route('student_ajax') }}', {term:''}, function(students){
			$("#students").html(''); // clean options
			if ( students.length ) {
				$.each( students, function(index,pupil)  {
					$("#students").append('<option value="'+pupil.id+'">'+pupil.first_name+' '+pupil.last_name+'</option>');
				});
			}
		});

		var isChosen = false;
		$('#target').change(function (e) {
			e.preventDefault();

			switch( $(this).val() ){
				case 'students':
					
					if ( isChosen ) { $("#students").chosen("destroy"); }
					isChosen = false;

					$('#college_id').removeClass('hidden');
					$('#target_organization').addClass('hidden');
					$('#students').addClass('hidden');

					$('#college_id').removeAttr('disabled');
					$('#target_organization').attr('disabled','disabled');
					$('#students').attr('disabled','disabled');

				break;
				case 'officers':

					if ( isChosen ) { $("#students").chosen("destroy"); }
					isChosen = false;

					$('#college_id').addClass('hidden');
					$('#target_organization').removeClass('hidden');
					$('#students').addClass('hidden');

					$('#college_id').attr('disabled','disabled');
					$('#target_organization').removeAttr('disabled');
					$('#students').attr('disabled','disabled');
				
				break;
				case 'individual':

					$('#college_id').addClass('hidden');
					$('#target_organization').addClass('hidden');
					$('#students').removeClass('hidden');

					$('#college_id').attr('disabled','disabled');
					$('#target_organization').attr('disabled','disabled');
					$('#students').removeAttr('disabled');

					$('#students').chosen({});
					isChosen = true;

				break;
			}
		});
	});
</script>
@endsection