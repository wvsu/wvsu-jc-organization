@extends('layouts.admin')

@section('content')

<div class="col-md-12">
	<div class="ibox">
		<div class="ibox-title">
		    <h5>All events available for the university</h5>
		    <div class="ibox-tools">
		        {{-- <a href="{{ route('events.create') }}" class="btn btn-primary btn-xs">Create new event</a> --}}
		    </div>
		</div>
		<div class="ibox-content">
		    <div class="row m-b-sm m-t-sm">
		        <div class="col-md-1">
		            <a type="button" href="{{ route('events.index') }}" id="loading-example-btn" class="btn btn-white btn-sm"><i class="fa fa-refresh"></i> Refresh</a>
		        </div>
		        <div class="col-md-11">
		            {{-- <div class="input-group"><input type="text" placeholder="Search" class="input-sm form-control"> <span class="input-group-btn">
		                <button type="button" class="btn btn-sm btn-primary"> Go!</button> </span></div> --}}
		        </div>
		    </div>

		    <div class="project-list">
				
				@if( $events->count() )
		        <table class="table table-hover">
		            <tbody>
	            	@foreach( $events as $event )
	            		<?php 
                        $is_done  = ( strtotime( $event->end_date ) < strtotime( date('Y-m-d H:i:s') ) ); 
                        $is_today = ( strtotime( $event->end_date ) == strtotime( date('Y-m-d H:i:s') ) ); 
                        $success  = ( $is_done ) ? 'default' : 'success';
                        $status   = ( $is_done ) ? 'Finished' : 'Upcoming';
                        if ( $is_today ) {
                            $success = 'primary';
                            $status  = 'Active';
                        }
                        ?>
			            <tr>
			                <td class="project-status">
			                    <span class="label label-{{ $success }}">{{ $status }}</span>
			                </td>
			                <td class="project-title">
			                    <a href="{{ route('events.show', $event->id) }}">{{ $event->name }}</a>
			                    <br>
			                    <small>
			                    	<b>Start:</b> {{ date("M.d.Y, g:i a" , strtotime($event->start_date) ) }} 
			                    	<b>End:</b> {{ date("M.d.Y, g:i a" , strtotime($event->end_date) ) }}
			                    </small>
			                </td>
			                <td class="project-completion">
			                        <small>Completion with: 48%</small>
			                        <div class="progress progress-mini">
			                            <div style="width: 48%;" class="progress-bar"></div>
			                        </div>
			                </td>
			                <td class="project-people">
			                    {{ $event->org()->name }} ( {{ $event->org()->abrevation }} )
			                </td>
			                <td class="project-actions">
			                    <a href="{{ route('events.show', $event->id) }}" class="btn btn-white btn-sm"><i class="fa fa-folder"></i> View </a>
			                    <a href="{{ route('events.edit', $event->id) }}" class="btn btn-white btn-sm"><i class="fa fa-pencil"></i> Edit </a>
			                </td>
			            </tr>
		            @endforeach
		            {{-- 
		            <tr>
		                <td class="project-status">
		                    <span class="label label-default">Unactive</span>
		                </td>
		                <td class="project-title">
		                    <a href="project_detail.html">Many desktop publishing packages and web</a>
		                    <br>
		                    <small>Created 10.08.2014</small>
		                </td>
		                <td class="project-completion">
		                    <small>Completion with: 8%</small>
		                    <div class="progress progress-mini">
		                        <div style="width: 8%;" class="progress-bar"></div>
		                    </div>
		                </td>
		                <td class="project-people">
		                    <a href="">Information Technology Students Society</a>
		                </td>
		                <td class="project-actions">
		                    <a href="#" class="btn btn-white btn-sm"><i class="fa fa-folder"></i> View </a>
		                    <a href="#" class="btn btn-white btn-sm"><i class="fa fa-pencil"></i> Edit </a>
		                </td>
		            </tr> --}}
		            </tbody>
		        </table>

		        {{ $events->appends(request()->input())->links() }}
		        @else
		        <div class="alert alert-warning">
		        	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		        	<strong>No events found</strong> please try again or create a new one.
		        </div>
		        @endif
		    </div>
		</div>
	</div>
</div>

<div class="modal inmodal" id="remove-user" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Remove Category</h4>
            </div>
            <div class="modal-body">
                <p><strong>Are you sure?</strong> once removed, all levels and questions affiliated with this category will be deleted. This action is irreversible.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="pull-left btn btn-white"  data-dismiss="modal">Cancel</button>
                <button type="button" id="remove-btn" class="btn btn-danger">Delete</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('action')
<div class="title-action">
    <a href="{{ route('events.create') }}" class="btn btn-primary">Create Event <i class="fa fa-plus"></i></a>
</div>
@endsection

@section('styles')
<!-- Toastr style -->
<link href="{{asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- Toastr script -->
<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>
<script>
jQuery(document).ready(function() {

	toastr.options = {
		"closeButton": true,
		"debug": false,
		"progressBar": true,
		"positionClass": "toast-top-right",
		"onclick": null,
		"showDuration": "400",
		"hideDuration": "1000",
		"timeOut": "7000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}

	jQuery('button[data-name="delete"]').click(function(e) {
		e.preventDefault();
		var token = jQuery('meta[name="csrf-token"]').attr('content');
	});

	jQuery('#remove-user').on('show.bs.modal', function (event) {
		var button = jQuery(event.relatedTarget); // Button that triggered the modal
		var cat_id = button.data('id');
		jQuery('#remove-btn').data('id',cat_id);
  		jQuery(this).find('.modal-title').text('Remove ' + button.data('cat') );
	});

	jQuery('#remove-btn').click(function(e) {
		e.preventDefault();
		var cat_id = jQuery(this).data('id');
		var token = jQuery('meta[name="csrf-token"]').attr('content');

		jQuery.ajax({  
			url: '{{ route('events.index') }}/'+cat_id,
			type: 'POST',
			dataType: 'json',
			data: { _token: token, _method: 'DELETE' },
		})
		.always(function(data) {
			if ( data.error ) {
				toastr.error(data.message,'Error');
			} else {
				jQuery('#remove-user').modal('toggle');
				toastr.success(data.message,'Success');
				jQuery('#user-row-'+cat_id).remove();
			}
		});
		 
	});

});
</script>
@endsection
