@extends('layouts.admin')

@section('content')

<div class="col-lg-12">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="ibox">
			<div class="ibox-content">
				<div class="row">
					<div class="col-lg-12">
						<div class="m-b-md">
							<a href="{{ route('events.edit', $event->id) }}" class="btn btn-white btn-xs pull-right">Edit event</a>
							<h2>{{ $event->name }}</h2>
						</div>
						<dl class="dl-horizontal">
							<dt>Status:</dt> <dd><span class="label label-{{ $class }}">{{ $status }}</span></dd>
						</dl>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-5">
						<dl class="dl-horizontal">
							<dt>Created by:</dt> <dd>{{ $event->user()->first_name.' '.$event->user()->last_name }}</dd>
							<dt>Participants:</dt> <dd><span class="text-navy"> {{ ( $event->college() ) ? $event->college()->abrevation.' Students':'All Students' }}</span> </dd>
							<dt>Start Date:</dt> <dd> 	{{ date("M d, Y h:i a" , strtotime($event->start_date) ) }} </dd>
							<dt>End Date:</dt> <dd>  {{ date( 'M d, Y h:i a', strtotime( $event->end_date ) ) }}</dd>
						</dl>
					</div>
					<div class="col-lg-7" id="cluster_info">
						<dl class="dl-horizontal">

							<dt>Created:</dt> <dd> 	{{ date( 'd.m.Y h:i a', strtotime( $event->created_at ) ) }}</dd>
							<dt>Last Updated:</dt> <dd>{{ date( 'd.m.Y h:i a', strtotime( $event->updated_at ) ) }}</dd>
							<dt>Organization:</dt> <dd>{{ $event->org()->name }}</dd>
							<dt>Event Key:</dt> <dd><span class="text-navy">{{ $event->event_key }}</span></dd>
							{{-- <dt>Organization:</dt>
							<dd class="project-people">
								<span class="text-navy">{{ $event->org()->name }}</span>
							</dd> --}}
						</dl>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<dl class="dl-horizontal">
							<dt>Logged In:</dt>
							<dd>
								<div class="progress progress-striped active m-b-sm">
									<div style="width: {{ $present_percentage }}%;" class="progress-bar progress-bar-default"></div>
									@if( !$event->onetime )
									<div style="width: {{ $loggedout_percentage }}%;" class="progress-bar progress-bar-info"></div>
									@endif
									<div style="width: {{ $absent_percentage }}%;" class="progress-bar progress-bar-danger"></div>
								</div>
								<small> Legend: 
									<span class="label label-primary">Present {{ ceil( $present_percentage ) }}%</span>
									@if( !$event->onetime )
									<span class="label label-info">Never Logged Out {{ ceil( $loggedout_percentage ) }}%</span>
									@endif
									<span class="label label-danger">Absent {{ ceil( $absent_percentage ) }}%</span>
								</small>
								<br>
							</dd>							
						</dl>
					</div>
				</div>
				<div class="row m-t-sm">
					<div class="col-lg-12">
						<div class="panel blank-panel">

							<div class="panel-heading">
								<div class="panel-options">
									<ul class="nav nav-tabs">
										<li class="{{ $tab ? '':'active' }}"><a href="#tab-1" data-toggle="tab">Event Description</a></li>
										<li class="{{ $tab ? 'active':'' }}"><a href="#tab-2" data-toggle="tab">Participants</a></li>
										<li class=""><a href="#tab-3" data-toggle="tab">Comments</a></li>
									</ul>
								</div>
							</div>

							<div class="panel-body">

								<div class="tab-content">
									
									<div class="tab-pane{{ $tab ? '':' active' }}" id="tab-1">
										
										{!! $event->description !!}

									</div><!-- tab-1 -->

									<div class="tab-pane{{ $tab ? ' active':'' }}" id="tab-2">

										<form action="" method="GET" class="form-inline" role="form">
											<input type="hidden" name="tab" value="tab-2">

											@if( is_null( $event->college_id ) )
											<div class="input-group" style="margin-bottom: 15px;">
												<select class="form-control" name="college">
													<option value="">Select College</option>
													@foreach( $colleges as $college )
													<option value="{{ $college->id }}"{{ $college->id == $college_id ? ' selected':'' }}>{{ $college->name }}</option>
													@endforeach
												</select>
											</div>
											<br>
											@endif

											<div class="input-group">
												<span class="input-group-addon">Show</span> 
												<input type="number" name="perpage" style="width: 80px;" value="{{ $perpage }}" class="form-control">
											</div>
											
											<div class="input-group">
												<select class="form-control" name="year_level">
													<option value="">Year</option>
													<option value="1"{{ $level == '1' ? ' selected':'' }}>1st Year</option>
													<option value="2"{{ $level == '2' ? ' selected':'' }}>2nd Year</option>
													<option value="3"{{ $level == '3' ? ' selected':'' }}>3rd Year</option>
													<option value="4"{{ $level == '4' ? ' selected':'' }}>4th Year</option>
												</select>
											</div>

											<div class="input-group">
												<select class="form-control" name="year_section">
													<option value="">Section</option>
													<option value="A"{{ $section == 'A' ? ' selected':'' }}>Section A</option>
													<option value="B"{{ $section == 'B' ? ' selected':'' }}>Section B</option>
													<option value="C"{{ $section == 'C' ? ' selected':'' }}>Section C</option>
													<option value="D"{{ $section == 'D' ? ' selected':'' }}>Section D</option>
												</select>
											</div>

											<div class="input-group">
												<input type="text" placeholder="Name, Student ID" name="search" value="{{ $search }}" class="form-control"> 
												<span class="input-group-btn"> 
													<button type="submit" class="btn btn-info">
														Search <i class="fa fa-search"></i>
                                        			</button> 
                                        		</span>
                                    		</div>

                                    		<div class="input-group pull-right">
	                                    		<a href="{{ route('events.show', ['id'=>$event->id,'tab'=>'tab-2']) }}" class="btn btn-white">
													<i class="fa fa-refresh"></i> Reset Search
												</a>
											</div>

										</form>

										<hr>
										
										<p class="label label-default">
											Students found: <b>{{ $students->total() }}</b>
										</p>

										<br><br>

										@if( $students->count() )
										
										<div class="table-responsive">
											<table class="table table-hover table-bordered">
												<thead>
													<tr>
														@if( $event->target == 'officers' )
														<th>Position</th>
														@endif
														<th>First Name</th>
														<th>Last Name</th>
														<th>Year & Section</th>
														@if( $event->onetime )
														<th>Login Time</th>
														@else
														<th>Login Time</th>
														<th>Logout Time</th>
														@endif
														<th>Status</th>
													</tr>
												</thead>
												<tbody>
													@foreach( $students as $student )
													<tr class="{{ $statusClass[$student->status( $event->id, $student->id )['status']] }}">
														@if( $event->target == 'officers' )
														<td>{{ $student->officer()['name'] }}</td>
														@endif
														<td>{{ $student->first_name }}</td>
														<td>{{ $student->last_name }}</td>
														<td>{{ $student->year_level.'-'.$student->year_section }}</td>
														@if( $event->onetime )
														<td>
															@if( !is_null($student->status( $event->id, $student->id )['login']['created_at']) )
															{{ date('h:i A', strtotime( $student->status( $event->id, $student->id )['login']['created_at']  ) ) }}
															@endif
														</td>
														@else
														<td>
															@if( !is_null($student->status( $event->id, $student->id )['login']['created_at']) )
															{{ date('h:i A', strtotime( $student->status( $event->id, $student->id )['login']['created_at']  ) ) }}
															@endif
														</td>
														<td>
															@if( !is_null($student->status( $event->id, $student->id )['logout']['created_at']) )
															{{ date('h:i A', strtotime( $student->status( $event->id, $student->id )['logout']['created_at']  ) ) }}
															@endif
														</td>
														@endif
														<td>
															{{ ucfirst( $student->status( $event->id, $student->id )['status'] ) }}
														</td>
													</tr>
													@endforeach
												</tbody>
											</table>
										</div>
										
										{{ $students->appends( $args )->links() }}

										@else

										<div class="alert alert-warning">
											<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
											<strong>No Results Found,</strong> Please try a different seach.
										</div>

										@endif
									</div><!-- tab-2 -->

									<div class="tab-pane" id="tab-3">
										<div class="feed-activity-list">
											<div class="feed-element">
												<a href="#" class="pull-left">
													<img alt="image" class="img-circle" src="{{asset('img/profile_small.jpg')}}">
												</a>
												<div class="media-body ">
													<small class="pull-right">2h ago</small>
													<strong>Mark Johnson</strong> posted message on <strong>Monica Smith</strong> site. <br>
													<small class="text-muted">Today 2:10 pm - 12.06.2014</small>
													<div class="well">
														Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
														Over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
													</div>
												</div>
											</div>
											<div class="feed-element">
												<a href="#" class="pull-left">
													<img alt="image" class="img-circle" src="{{asset('img/profile_small.jpg')}}">
												</a>
												<div class="media-body ">
													<small class="pull-right">2h ago</small>
													<strong>Janet Rosowski</strong> add 1 photo on <strong>Monica Smith</strong>. <br>
													<small class="text-muted">2 days ago at 8:30am</small>
												</div>
											</div>
											<div class="feed-element">
												<a href="#" class="pull-left">
													<img alt="image" class="img-circle" src="{{asset('img/profile_small.jpg')}}">
												</a>
												<div class="media-body ">
													<small class="pull-right text-navy">5h ago</small>
													<strong>Chris Johnatan Overtunk</strong> started following <strong>Monica Smith</strong>. <br>
													<small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>
													<div class="actions">
														<a class="btn btn-xs btn-white"><i class="fa fa-thumbs-up"></i> Like </a>
														<a class="btn btn-xs btn-white"><i class="fa fa-heart"></i> Love</a>
													</div>
												</div>
											</div>
											<div class="feed-element">
												<a href="#" class="pull-left">
													<img alt="image" class="img-circle" src="{{asset('img/profile_small.jpg')}}">
												</a>
												<div class="media-body ">
													<small class="pull-right">2h ago</small>
													<strong>Kim Smith</strong> posted message on <strong>Monica Smith</strong> site. <br>
													<small class="text-muted">Yesterday 5:20 pm - 12.06.2014</small>
													<div class="well">
														Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
														Over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
													</div>
												</div>
											</div>
											<div class="feed-element">
												<a href="#" class="pull-left">
													<img alt="image" class="img-circle" src="{{asset('img/profile_small.jpg')}}">
												</a>
												<div class="media-body ">
													<small class="pull-right">23h ago</small>
													<strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>
													<small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>
												</div>
											</div>
											<div class="feed-element">
												<a href="#" class="pull-left">
													<img alt="image" class="img-circle" src="{{asset('img/profile_small.jpg')}}">
												</a>
												<div class="media-body ">
													<small class="pull-right">46h ago</small>
													<strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
													<small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
												</div>
											</div>
										</div>
									</div><!-- tab-3 -->

								</div><!-- tab-content -->

							</div><!-- panel-body -->

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

{{-- 
<div class="col-lg-4">
	<div class="wrapper wrapper-content project-manager">
		<h4>Event description</h4>
		<img src="img/organization_logo.png" class="img-responsive">
		<p class="small"></p>
	</div>
</div> --}}

@endsection

@section('action')
<div class="title-action">
<a href="{{ route('events.index') }}" class="btn btn-white"><i class="fa fa-chevron-left"></i> Back</a>
</div>
@endsection

@section('styles')
@endsection

@section('scripts')
@endsection

