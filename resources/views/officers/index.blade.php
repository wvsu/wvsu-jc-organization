@extends('layouts.admin')

@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Officers<small class="m-l-sm">Manage officers.</small></h5>
		</div>
		<div class="ibox-content">
			
			<form action="{{ route('officers.index') }}" method="GET" class="form-inline" role="form">
				<div class="input-group">
					<span class="input-group-addon">Per page</span> <input type="number" name="show" style="width: 80px;" placeholder="10" value="{{ $perpage }}" class="form-control">
				</div>
				
				<div class="input-group">
					<select name="org" class="form-control">
						<option value="">-- Select Organization</option>
						@foreach( $organizations as $organization )
							<option value="{{ $organization->id }}"{{ $organization->id == $org ? ' selected':'' }}>{{ ucwords( $organization->name ) }}</option>
						@endforeach
					</select>
				</div>

				<button type="submit" class="btn btn-info" style="margin: 0;">Filter <i class="fa fa-filter"></i></button>
			</form>
			<hr>

			@if( $officers->count() )

			<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover">
					<thead>
						<tr>
							<th>Position</th>
							<th>Organization</th>
							<th>Name</th>
							<th>College</th>
							<th>Year & Section</th>
							<th>Options</th>
						</tr>
					</thead>
					<tbody>
						@foreach( $officers as $officer )
							<tr id="user-row-{{ $officer->id }}">
								<td>{{ $officer->name }}</td>
								<td>{{ $officer->org()->abrevation }}</td>
								<td>{{ $officer->student()->first_name.' '.$officer->student()->last_name }}</td>
								<td>{{ $officer->student()->college()->name }}</td>
								<td>{{ $officer->student()->year_level.'-'.$officer->student()->year_section }}</td>
								<td>
									<a href="{{ route('officers.edit', $officer->id) }}" class="btn btn-success btn-xs">
										<i class="fa fa-edit"></i> Edit
									</a> | 
									<button data-id="{{ $officer->id }}" data-cat="{{ $officer->name }}" data-name="delete" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#remove-user">
										<i class="fa fa-close"></i> Delete
									</button>
								</td>
							</tr>

						@endforeach
					</tbody>
				</table>
			</div>
			
			{{ $officers->appends(request()->input())->links() }}

			@else 

			<div class="alert alert-warning">
				<strong>No records found,</strong> try again or start adding new officers.
			</div>

			@endif

		</div>
	</div>
</div>

<div class="modal inmodal" id="remove-user" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Remove Officer</h4>
            </div>
            <div class="modal-body">
                <p><strong>Are you sure?</strong> once removed, all details and information affiliated with this officer will be deleted. This action is irreversible.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="pull-left btn btn-white"  data-dismiss="modal">Cancel</button>
                <button type="button" id="remove-btn" class="btn btn-danger">Delete</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('action')
<div class="title-action">
    <a href="{{ route( 'officers.create' ) }}" class="btn btn-primary">Add Officer <i class="fa fa-plus"></i></a>
</div>
@endsection

@section('styles')
<!-- Toastr style -->
<link href="{{asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- Toastr script -->
<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>
<script>
jQuery(document).ready(function() {

	toastr.options = {
		"closeButton": true,
		"debug": false,
		"progressBar": true,
		"positionClass": "toast-top-right",
		"onclick": null,
		"showDuration": "400",
		"hideDuration": "1000",
		"timeOut": "7000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}

	jQuery('button[data-name="delete"]').click(function(e) {
		e.preventDefault();
		var token = jQuery('meta[name="csrf-token"]').attr('content');
	});

	jQuery('#remove-user').on('show.bs.modal', function (event) {
		var button = jQuery(event.relatedTarget); // Button that triggered the modal
		var cat_id = button.data('id');
		jQuery('#remove-btn').data('id',cat_id);
  		jQuery(this).find('.modal-title').text('Remove ' + button.data('cat') );
	});

	jQuery('#remove-btn').click(function(e) {
		e.preventDefault();
		var cat_id = jQuery(this).data('id');
		var token = jQuery('meta[name="csrf-token"]').attr('content');

		jQuery.ajax({  
			url: '{{ route('officers.index') }}/'+cat_id,
			type: 'POST',
			dataType: 'json',
			data: { _token: token, _method: 'DELETE' },
		})
		.always(function(data) {
			if ( data.error ) {
				toastr.error(data.message,'Error');
			} else {
				jQuery('#remove-user').modal('toggle');
				toastr.success(data.message,'Success');
				jQuery('#user-row-'+cat_id).remove();
			}
		});
		 
	});

});
</script>
@endsection
