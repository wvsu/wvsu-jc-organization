@extends('layouts.admin')

@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Register Officer<small class="m-l-sm"></small></h5>
		</div>
		<div class="ibox-content">

			<form action="{{ route('officers.store') }}" method="POST" role="form">
				{{ csrf_field() }}

				<div class="row">
					<div class="col-md-6">

						<div class="form-group">
							<label for="student_id">Organization</label>
							<select name="organization_id" class="form-control">
								<option value="">-- Select Organization</option>
								@foreach( $organizations as $organization )
									<option value="{{ $organization->id }}"{{ $organization->id == old('organization_id') ? ' selected':'' }}>{{ ucwords( $organization->name ) }}</option>
								@endforeach
							</select>
						</div>

						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label for="name">Designation/Position</label>
									<input type="text" value="{{ old('name') }}" name="name" class="form-control" id="name" placeholder="President, Asst. President, Auditor...">
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label for="rank">Rank Order</label>
									<input type="number" value="{{ old('rank') ? old('rank'): $rank }}" name="rank" class="form-control" id="rank" placeholder="0 - 100">
									<span><small>( <i>Position standing like 1 is equal to President</i> )</small></span>
								</div>
							</div>
						</div>

					</div>

					<div class="col-md-6">

						<div class="form-group">
							<label for="student_id">Student</label>
			                <select data-placeholder="Choose student..." name="student_id" class="chosen-select" id="student_id" style="width: 100%;">
			                	<option value="">Select Student</option>
			                </select>
						</div>

						<button type="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>

					</div>

				</div>
			
			</form>

		</div>
	</div>
</div>

@endsection

@section('action')
<div class="title-action">
<a href="{{ route('officers.index') }}" class="btn btn-white"><i class="fa fa-chevron-left"></i> Back</a>
</div>
@endsection


@section('styles')
<link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
<link href="{{asset('css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
<link href="{{asset('css/plugins/chosen/chosen.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- Data picker -->
<script src="{{asset('js/plugins/datapicker/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
<script>
	jQuery(document).ready(function($) {
		var $=jQuery;

		$('#data_3 .input-group.date').datepicker({
            startView: 2,
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true
        });

		$.getJSON('{{ route('student_ajax') }}', {term:''}, function(students){
			$.each( students, function(index,pupil)  {
				$("#student_id").append('<option value="'+pupil.id+'">'+pupil.first_name+' '+pupil.last_name+'</option>');
			});
			$('#student_id').chosen({});
		});
	});
</script>
@endsection