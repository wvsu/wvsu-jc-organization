<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
	
	if ( Auth::check() ) {
		return view('welcome');	

	} else {
		return redirect('login');
	}
});

// Mobile API
Route::get('api/log', 'ApiController@api_log')->name('api_log');
Route::get('qr/{size}/{format}/{key}', 'ApiController@qr')->name('qr');

Auth::routes();

// admin routes
Route::get('home', 'HomeController@index')->name('home');
Route::get('ajax/students', 'HomeController@student_ajax')->name('student_ajax');
Route::get('print', 'HomeController@print_qr')->name('print');
Route::get('print/students', 'HomeController@print_students')->name('print_students');

// settings
Route::get('settings', 'SettingsController@index')->name('settings');
Route::post('settings/profile', 'SettingsController@profile_update')->name('profile.update');
Route::post('settings/password', 'SettingsController@password_update')->name('password.update');

// Restful Controllers
Route::resources([
	'users' => 'UsersController',
	'roles' => 'RolesController',
	'events' => 'EventsController',
	'colleges' => 'CollegesController',
	'students' => 'StudentsController',
	'organizations' => 'OrganizationsController',
	'officers' => 'OfficersController',
]);


